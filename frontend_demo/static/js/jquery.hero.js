/*
 *  Project: Hero
 *  Description: jQuery Hero Widget
 *  Author: Jason Yang
 *  License: MIT
 *  @see http://jqueryboilerplate.com/
 */

;
(function ($, window, document, undefined) {
    var pluginName = "hero",
        defaults = {
            height: 450,
            width: '100%',
            delay: 5000,
            speed: 800,
            transition: 'fade',
            nav: true
        };

    // The actual plugin constructor
    function Plugin(element, options) {
        this.element = element;
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;

        this.wrapper = this.element.children[0];
        this.items = this.wrapper.children;
        this.currentIndex = -1;
        this.timer;

        this.init();
    }

    Plugin.prototype = {

        init: function () {
            var $this = this;
            // Dimensions
            $(this.element).width(this.options.width).height(this.options.height);
            var $this = this;
            for (var i = 0; i < this.items.length; i++) {
                var item = $(this.items[i]);
                item.css({
                    'visibility': 'hidden',
                    'opacity': 0,
                    'transition': 'all ' + (this.options.speed / 1000) + 's'
                });

                // Handle image load
                this.getImageSize($('img', item).prop('src'), this, function (dim) {
                    this.centerImage($('img', item), item);
                });
            }

            // Navigation
            if (this.options.nav) {
                this.nav = $('<nav></nav>').appendTo(this.element);
                for (var i = 0; i < this.items.length; i++) {
                    $('<a href=""></a>').on('click', { context: this, index: i },function (event) {
                        event.data.context.loadItem(event.data.index);
                        return false;
                    }).appendTo(this.nav);
                }
            }

            // Window Resizing
            $(window).on('resize', { context: this }, function (event) {
                event.data.context.resize();
            });
            $(window).resize();

            // Load First item
            $(window).on('load', { context: this}, function (event) {
                event.data.context.loadItem(0);
            });

            // Timer
            if (this.options.delay > 0) {
                this.start();
                $(this.element).hover(
                    function() {
                        $this.stop();
                    },
                    function() {
                        $this.start();
                    }
                );
            }
        },

        start: function() {
//            console.log('Starting Interval Timer');
            var $this = this;
            this.timer = setInterval( function() {
                $this.next.call($this);
            }, this.options.delay);
        },

        stop: function() {
//            console.log('Stopping Interval Timer');
            clearInterval(this.timer);
        },

        resetTimer: function() {
            this.stop();
            this.start();
        },

        next: function() {
            this.loadItem(this.currentIndex + 1);
        },

        loadItem: function (index, callback) {
            if (index >= this.items.length) {
                index = 0;
            } else if (index < 0) {
                index = this.items.length - 1;
            }

            if (this.options.nav) {
                this.nav.children().eq(index).addClass('current').siblings().removeClass('current');
            }

            // Get current and new item
            var oldItem = $(this.items[this.currentIndex]);
            var newItem = $(this.items[index]);

            // Prepare current and new item
            oldItem.css('zIndex', 10);
            newItem.css({'zIndex': 20, 'visibility': 'visible', 'opacity': 0});

            // Do it!
            this.currentIndex = index;
            var $this = this;

            newItem.css({
                'opacity': 1
            });
            newItem.one('webkitTransitionEnd transitionend msTransitionEnd oTransitionEnd', function() {
                oldItem.css({
                    'visibility': 'hidden',
                    'zIndex': 10,
                    'opacity': 0
                });
                if ($.isFunction(callback)) {
                    callback.call($this);
                }
            });
        },

        resize: function () {
            for (var i = 0; i < this.items.length; i++) {
                $(this.items[i]).width($(this.element).width());
                this.centerImage($('> *', this.items[i]), $(this.items[i]));
            }
        },

        centerImage: function (target, container) {
            var container_ratio = container.width() / container.height();
            var target_ratio = target.width() / target.height();
            var image_width, image_height, image_top, image_left;
            if (container_ratio > target_ratio) {
                image_width = container.width();
                image_height = image_width / target_ratio;
                image_top = (container.height() - image_height) / 2;
                image_left = 0;
            } else {
                image_height = container.height();
                image_width = image_height * target_ratio;
                image_top = 0;
                image_left = (container.width() - image_width) / 2;
            }
            var imageCss = {
                width: Math.round(image_width),
                height: Math.round(image_height),
                top: Math.round(image_top),
                left: Math.round(image_left)
            };
            target.css(imageCss);
        },

        getImageSize: function (imageSrc, context, callback) {
            var newImg = new Image();

            newImg.onload = function () {
                var dim = {
                    width: newImg.width,
                    height: newImg.height
                };

                callback.call(context, dim);
            }
        }
    };

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            }
        });
    };

})(jQuery, window, document);