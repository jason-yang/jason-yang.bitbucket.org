/* Elements to hide when clicking on document: Just add jquery object to array to add to list */
var elementsToHideOnClickDocument = [];

/**
 * Hide all stuff on click
 */
function hideAll() {
    for (var i = 0; i < elementsToHideOnClickDocument.length; i++) {
        elementsToHideOnClickDocument[i].hide();
    }
}

jQuery( function($) {
    elementsToHideOnClickDocument.push($('#top-nav ul ul')); // Hide all submenus if clicking on document

    /* Top Nav */
    $('#top-nav li').each( function() {
        var $this = $(this);
        var submenu = $this.children('ul');
        if (submenu.length > 0) {
            var link = $('> a', $this);
            link.addClass('hasMenu');
            var showMenu = function() {
                submenu.show();
                return false;
            };

            var showMenuTimer, hideMenuTimer;
            $this.children('a').click(false); // Disable clicks
            $this.hover( function() {
                submenu.show();
                clearTimeout(hideMenuTimer);
            }, function() {
                hideMenuTimer = setTimeout(function() {
                    submenu.hide();
                }, 400);
            });

        }
    });

    /* Search Box */
    var searchShowTimer, searchHideTimer;
    $('#search').on('click', function() {
        $(this).addClass('active', 400, function() {
            $('input', this).focus();
        });
    });

    $('#search input').focus( function() {
        var input = $(this);
        if (input.val() == input.prop('title')) {
            input.val('');
        }
        input.addClass('active');
    }).blur( function() {
        var input = $(this);
        if (input.val() == '') {
            input.val(input.prop('title'));
            searchHideTimer = setTimeout( function() {
                clearTimeout(searchShowTimer);
                $('#search').removeClass('active', 200);
            }, 2000);
        }
    });

    /* Hero */
    $('#hero').hero({
        height: 600
    });

    $(document).click(hideAll);
    $(window).resize();
});